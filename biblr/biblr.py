import os
import io
import bz2
import csv
import sqlite3
from pathlib import Path

from flask import (
    Flask, request, session, g, redirect, url_for, abort,
    render_template, flash
)
import requests
# books module
from . import books

app = Flask(__name__)
app.config.from_object(__name__)

app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'biblr.db'),
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('BIBLR_SETTINGS', silent=True)


def connect_db():
    '''Connects to the specific database.'''
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def get_db():
    '''Opens a new database connection if there is none yet for the
    current application context.

    '''
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    '''Closes the database again at the end of the request.'''
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

# handy code to hold down all verses and write to a local file
def kjv_verses():
    kjv_path = Path(app.root_path, 'kjv.tsv.bz2')
    if not kjv_path.exists():
        data = requests.get(
            'https://belhavencs.nyc3.digitaloceanspaces.com/csc211/kjv.tsv.bz2'
        ).content
        kjv_path.write_bytes(data)
        kjv_data = io.BytesIO(data)
    else:
        kjv_data = io.BytesIO(kjv_path.read_bytes())

    with bz2.open(kjv_data, 'rt') as rfp:
        columns = ['book','chapter','verse','text']
        for row in csv.DictReader(rfp, columns, delimiter='\t'):
            yield row

# inserting verses (lots of things into a table at once)
def insert_verses(cursor):
    rows = list(kjv_verses())
    cursor.executemany(
        'insert into kjv (book,chapter,verse,text) values'
        ' (:book,:chapter,:verse,:text)', rows
        # first thing in db row will be (:book,:chapter,:verse,:text)
    )


def init_db():
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    insert_verses(db.cursor())
    db.commit()


@app.cli.command('initdb')
def initdb_command():
    '''Initializes the database.'''
    init_db()
    app.logger.info('Initialized the database')

# make books module avaibale too me to have a short hand for all my books as keys for long hand
@app.context_processor
def inject_books():
    return {'books': books}


@app.route('/')
def index():
    db = get_db()
    return render_template('index.html')


def setup_show_verses(book, chapter, verse):
    g.book = book
    g.chapter = chapter
    g.verse = verse

    # Build the breadcrumb
    bc = [({'book':book}, books.BOOK_LUT[book])]
    if chapter is not None:
        bc.append(({'chapter': chapter, 'book': book}, chapter + 1))
    if verse is not None:
        bc.append(({'chapter': chapter, 'book': book, 'verse': verse},
                   verse + 1))
    g.breadcrumb = [(url_for('show_verses',**kw), v) for kw,v in bc]

    # Grab the verses from the database
    db = get_db()
    command = 'select book, chapter, verse, text from kjv where {}'

    where = [('book',book),('chapter',chapter),('verse',verse)]
    where_columns = [col for col,value in where if value is not None]
    where_values = [value for col,value in where if value is not None]

    where_str = ' and '.join('{}=?'.format(col) for col in where_columns)
    command = command.format(where_str)

    cur = db.execute(command, where_values)
    g.verses = cur.fetchall()


# routing three routes
@app.route('/<book>')
@app.route('/<book>/<int:chapter>')
@app.route('/<book>/<int:chapter>/<int:verse>')
def show_verses(book, chapter=None, verse=None):
    setup_show_verses(book, chapter, verse)

    # ----------------------------------------------------------------
    # Your solution starts below this comment section
    # ----------------------------------------------------------------
    #
    # - You need to load the all comments (book, chapter, verse, and
    #   text) from the comments table
    #
    # - You then need to prepare a dictionary whose keys are a tuple
    #   (book, chapter, verse) and values are a list of comments
    #   associated with that book, chapter, and verse
    #
    # - You then need to pass this dictionary to the render_template
    #   function under the keyword "comments"
    #
    # pull all comments and store it into rand var.
    # ccreate form of a dictionary (book, (int)chapter, (int)verse)

    # grabbing the comments
    db = get_db()
    command = 'select book, chapter, verse, text from comments where {}'

    where = [('book',book),('chapter',chapter),('verse',verse)]
    where_columns = [col for col,value in where if value is not None]
    where_values = [value for col,value in where if value is not None]

    where_str = ' and '.join('{}=?'.format(col) for col in where_columns)
    command = command.format(where_str)

    cur = db.execute(command, where_values)
    comments = cur.fetchall()

    def make_dicts(cur, comments):
        return dict((cur.description[idx][0], value)
                for idx, value in enumerate(comments))

    db.row_factory = make_dicts(cur, comments)
    db.row_factory = sqlite3.Row
    comments = db.row_factory

    # dict1 = [{setup_show_verses : list_comments}]
    # { _key : _value(_key) for _key in _container }
    # x = { row.SITE_NAME : row.LOOKUP_TABLE for row in cursor }

    # comments = {where: comments for row in cur.fetchall()}


    return render_template(
        'show_verses.html', comments=comments
    )

@app.route('/search', methods=['POST'])
def search():
    return redirect(url_for('index'))


@app.route('/add', methods=['POST'])
def add_comment():
    if not session.get('logged_in'):
        abort(401)

    # ----------------------------------------------------------------
    # Your solution starts below this comment section
    # ----------------------------------------------------------------
    #
    # - You need to get the form values for the following keys:
    #   - 'book'
    #   - 'chapter'
    #   - 'verse'
    #   - 'text'
    #
    # - 'chapter' and 'verse' need to be correctly converted to the
    #   type (and value) expected by the comments table
    #
    # - You then need to insert the (book, chapter, verse, text)
    #   values into the comments table
    #
    # convert John:0 to John:1

    db = get_db()
    # inserting chagnes into db

    db.execute('insert into comments (book, chapter, verse, text) values (?, ?, ?, ?)',
        [request.form['book'], request.form['chapter'], request.form['verse'], request.form['text']]
    )

    # save changes through commit()
    db.commit()

    flash('New entry was successfully posted')
    return redirect(request.args.get('next') or
                    request.referrer or url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
            origin = request.args.get('origin')
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
            origin = request.args.get('origin')
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(request.args.get('origin'))
    origin = request.referrer
    return render_template('login.html', error=error, origin=origin)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(request.referrer)
